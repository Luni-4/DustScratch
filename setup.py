# DustScratch - Film Stock Editor
# Copyright (C) 2018 Michele Valsesia

from setuptools import find_packages, setup

setup(
    author="Michele Valsesia",
    name="DustScratch",
    long_description="Film Stock Editor",
    version="0.0",
    url="https://gitlab.com/Luni-4/DustScratch",
    packages=find_packages(),
    entry_points={"console_scripts": ["DustScratch = DustScratch.__main__:main"]},
    package_dir={"DustScratch": "DustScratch"},
    install_requires=[
        "numpy",
        "PyQT5",
        "quamash",
        "opencv-python",
        "rawpy",
        "vapoursynth",
    ],
    classifiers=[
        "Environment :: X11 Applications :: Qt",
        "Development Status :: 4 - Beta",
        "Intended Audience :: End Users/Desktop",
        "Natural Language :: English",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 3 :: Only",
        "Topic :: Multimedia :: Video",
    ],
)
