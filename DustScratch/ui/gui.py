# DustScratch - Film Stock Editor
# Copyright (C) 2018 Michele Valsesia

from PyQt5 import QtCore

from DustScratch.processor.commands import CommandsProcessor
from DustScratch.processor.filters import FiltersProcessor
from DustScratch.processor.log import LogProcessor
from DustScratch.processor.project import ProjectProcessor
from DustScratch.processor.shortcuts import ShortcutsProcessor
from DustScratch.processor.undo import UndoProcessor
from DustScratch.processor.video import VideoProcessor


class Gui(QtCore.QObject):

    quit_requested = QtCore.pyqtSignal()

    def __init__(self):

        super().__init__()

        self.project = ProjectProcessor(self)
        self.command = CommandsProcessor(self)
        self.filters = FiltersProcessor(self)
        self.video = VideoProcessor(self)
        self.undo = UndoProcessor(self)
        self.log = LogProcessor()
        self.shortcuts = ShortcutsProcessor()

        self.main_window = None

    def set_main_window(self, main_window):
        self.main_window = main_window

    async def exec(self, func, *args, **kwargs):
        return await func(self.main_window, *args, **kwargs)

    def quit(self):
        self.quit_requested.emit()
