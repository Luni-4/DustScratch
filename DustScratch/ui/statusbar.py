# DustScratch - Film Stock Editor
# Copyright (C) 2018 Michele Valsesia

from PyQt5 import QtWidgets

import DustScratch.ui.gui


class StatusBar(QtWidgets.QStatusBar):
    def __init__(self, gui, parent):
        super().__init__(parent)
        self._gui = gui

        self.setSizeGripEnabled(False)
        self.setStyleSheet("font: 12pt;")

        self._video_frame_label = QtWidgets.QLabel(self)
        self._video_frame_label.setFrameStyle(
            QtWidgets.QFrame.Panel | QtWidgets.QFrame.Sunken
        )
        self._video_frame_label.setLineWidth(1)

        self._video_mode_label = QtWidgets.QLabel(self)
        self._video_mode_label.setFrameStyle(
            QtWidgets.QFrame.Panel | QtWidgets.QFrame.Sunken
        )
        self._video_mode_label.setLineWidth(1)

        self._video_zoom_label = QtWidgets.QLabel(self)
        self._video_zoom_label.setFrameStyle(
            QtWidgets.QFrame.Panel | QtWidgets.QFrame.Sunken
        )
        self._video_zoom_label.setLineWidth(1)

        self.addPermanentWidget(self._video_mode_label)
        self.addPermanentWidget(self._video_zoom_label)
        self.addPermanentWidget(self._video_frame_label)

        gui.video.show_frame.connect(self._show_frame_index)
        gui.video.zoom_frame.connect(self._show_zoom)
        gui.video.show_mode.connect(self._show_video_mode)

    def _show_frame_index(self):
        if self._gui.video.is_open:
            self._video_frame_label.setText(f"Frame: {self._gui.video.frame_idx}")
            self._show_zoom()
            self._show_video_mode()
        else:
            self._video_frame_label.clear()
            self._video_zoom_label.clear()
            self._video_mode_label.clear()

    def _show_zoom(self):
        perc = str(self._gui.video.zoom * 100).replace(".0", "")
        self._video_zoom_label.setText(f"Zoom: {perc}%")

    def _show_video_mode(self):
        if self._gui.video.mode:
            self._video_mode_label.setText("Mode: Filtered")
        else:
            self._video_mode_label.setText("Mode: Original")
