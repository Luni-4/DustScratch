# DustScratch - Film Stock Editor
# Copyright (C) 2018 Michele Valsesia

from PyQt5 import QtCore, QtGui, QtWidgets

from DustScratch.ui.classes.frame_label import FrameLabel
from DustScratch.ui.classes.mouse_slider import MouseSlider


class _VideoPreview(QtWidgets.QWidget):
    def __init__(self, gui, parent):
        super().__init__(parent)
        self._gui = gui

        # self._splash()
        self._splash_image = QtGui.QImage(720, 480, QtGui.QImage.Format_RGB888)
        self._splash_image.fill(0)

        # Frame Label
        self.frame_label = FrameLabel(self)
        self.frame_label.setStyleSheet("background-color:black;")
        self.frame_label.setPixmap(QtGui.QPixmap.fromImage(self._splash_image))

        # Scroll Area
        self.frame_scroll = QtWidgets.QScrollArea(self)
        self.frame_scroll.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.frame_scroll.setFocusPolicy(QtCore.Qt.NoFocus)
        self.frame_scroll.setAlignment(QtCore.Qt.AlignCenter)
        self.frame_scroll.setWidgetResizable(True)
        self.frame_scroll.setWidget(self.frame_label)

        vbox = QtWidgets.QVBoxLayout(self)
        vbox.setContentsMargins(0, 0, 0, 0)
        vbox.addWidget(self.frame_scroll)

        self._gui.video.show_frame.connect(self._show_frame)

        self._gui.video.frame_changed.connect(self._reset_path)

        self._gui.video.manual_mode.connect(self._set_manual)

        self._gui.video.mask_sig.connect(self._get_mask)

    def sizeHint(self):
        return QtCore.QSize(720, 480)

    def _set_manual(self):
        self.frame_label.setMode(self._gui.video.is_manual)

    def _get_mask(self):
        self._gui.video.set_mask(self.frame_label.getPathImage())

    def _reset_path(self):
        self.frame_label.resetPath()

    def _show_frame(self):
        if self._gui.video.curr_frame is None:
            self.frame_label.setPixmap(self._splash_image)
        else:
            image = QtGui.QImage(
                self._gui.video.curr_frame,
                self._gui.video.width,
                self._gui.video.height,
                QtGui.QImage.Format_RGB888,
            )

            self.frame_label.setZoom(self._gui.video.zoom)

            self.frame_label.setPixmap(QtGui.QPixmap.fromImage(image))

    def _splash(self):
        _FILENAME = "DustScratch/data/mips.png"
        self._splash_image = QtGui.QImage(
            QtCore.QDir.currentPath() + QtCore.QDir.separator() + _FILENAME
        )

        self._splash_image = self._splash_image.scaled(
            720, 480, QtCore.Qt.KeepAspectRatio
        )
        self._splash_image.convertToFormat(QtGui.QImage.Format_RGB32)


class _VideoSlider(QtWidgets.QWidget):
    def __init__(self, gui, parent):
        super().__init__(parent)
        self._gui = gui

        # Frame Slider
        self.frame_slider = MouseSlider(self)
        self.frame_slider.setOrientation(QtCore.Qt.Horizontal)
        self.frame_slider.setTracking(False)
        self.frame_slider.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.frame_slider.setSingleStep(1)

        vbox = QtWidgets.QVBoxLayout(self)
        vbox.addWidget(self.frame_slider)

        self._connect_ui_signals()

        self._gui.video.opened.connect(self._video_set_range)

        self._gui.video.frame_changed.connect(self._video_frame_change)

    def _connect_ui_signals(self):
        self.frame_slider.valueChanged.connect(self._frame_slider_value_change)

    def _disconnect_ui_signals(self):
        self.frame_slider.valueChanged.disconnect(self._frame_slider_value_change)

    def _frame_slider_value_change(self):
        self._gui.video.get_frame(self.frame_slider.value())

    def _video_frame_change(self):
        self._disconnect_ui_signals()
        self.frame_slider.setValue(self._gui.video.frame_idx)
        self._gui.video.show_frame.emit()
        self._connect_ui_signals()

    def _video_set_range(self):
        self.frame_slider.setMinimum(0)
        self.frame_slider.setMaximum(self._gui.video.frame_count - 1)
        self.frame_slider.setPageStep(self._gui.video.frame_count * 20 / 100)
        self._gui.video.get_frame(0)


class Video(QtWidgets.QWidget):
    def __init__(self, gui, parent):
        super().__init__(parent)

        self._gui = gui
        self._parent = parent

        self._video_preview = _VideoPreview(gui, self)
        self._video_slider = _VideoSlider(gui, self)

        vbox = QtWidgets.QVBoxLayout(self)
        vbox.setSpacing(2)
        vbox.setContentsMargins(0, 0, 0, 0)
        vbox.addWidget(self._video_preview)
        vbox.addWidget(self._video_slider)
