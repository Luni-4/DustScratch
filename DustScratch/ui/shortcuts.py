# DustScratch - Film Stock Editor
# Copyright (C) 2018 Michele Valsesia

import functools

from PyQt5 import QtGui, QtWidgets

from DustScratch.processor.shortcuts import ShortcutContext


def create_shortcuts(gui, context_widget, shortcuts_map):

    main_widget = context_widget[ShortcutContext.Global]
    for shortcut in main_widget.findChildren(QtWidgets.QShortcut):
        shortcut.setParent(None)

    key_sequences = []
    command_map = {}

    for context, shortcuts in shortcuts_map.items():
        parent = context_widget[context]
        for shortcut in shortcuts:
            command = gui.command.get_command(shortcut.command)
            command_map[parent, shortcut.shortcut] = command
            key_sequences.append(shortcut.shortcut)

    def _on_activate(keys):
        widget = QtWidgets.QApplication.focusWidget()
        while widget:
            if (widget, keys) in command_map:
                gui.command.run_command(command_map[widget, keys])
                break
            widget = widget.parent()

    for key_sequence in key_sequences:
        shortcut = QtWidgets.QShortcut(QtGui.QKeySequence(key_sequence), main_widget)
        shortcut.activatedAmbiguously.connect(shortcut.activated.emit)
        shortcut.activated.connect(functools.partial(_on_activate, key_sequence))
