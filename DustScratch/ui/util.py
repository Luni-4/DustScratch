# DustScratch - Film Stock Editor
# Copyright (C) 2018 Michele Valsesia

import asyncio

import numpy as np
from PyQt5 import QtCore, QtWidgets


def convert_QImage_to_array(image, colorspace):
    image = image.convertToFormat(colorspace)

    ptr = image.bits()
    ptr.setsize(image.byteCount())
    return np.array(ptr).reshape(image.height(), image.width(), 1)


def create_spinbox(label_name, minimum, maximum, step, parent=None):
    spinbox_label = QtWidgets.QLabel(label_name, parent)

    spinbox = QtWidgets.QSpinBox(parent)
    spinbox.setMinimum(minimum)
    spinbox.setMaximum(maximum)
    spinbox.setSingleStep(step)

    hbox = create_hwidget_layout(None, spinbox_label, spinbox)
    return (spinbox, hbox)


def create_double_spinbox(label_name, minimum, maximum, step, parent=None):
    spinbox_label = QtWidgets.QLabel(label_name, parent)

    spinbox = QtWidgets.QDoubleSpinBox(parent)
    spinbox.setMinimum(minimum)
    spinbox.setMaximum(maximum)
    spinbox.setSingleStep(step)

    hbox = create_hwidget_layout(None, spinbox_label, spinbox)
    return (spinbox, hbox)


def create_groupbox(group_name, parent, layout):
    group = QtWidgets.QGroupBox(group_name, parent)
    group.setAlignment(QtCore.Qt.AlignHCenter)
    group.setLayout(layout)

    return group


def create_grid_groupbox(group_name, parent, *layouts):
    grid = QtWidgets.QGridLayout()
    for layout in layouts:
        grid.addLayout(*layout)

    return create_groupbox(group_name, parent, grid)


def create_radio_button_group(group_name, parent, *buttons):
    label = QtWidgets.QLabel(group_name, parent)

    button_group = QtWidgets.QButtonGroup(parent)
    button_saver = []
    for button in buttons:
        radio_button = QtWidgets.QRadioButton(button[0], parent)
        button_saver.append(radio_button)
        button_group.addButton(radio_button, button[1])

    hbox = create_hwidget_layout(None, label, *button_saver)

    return (button_group, hbox)


def create_hwidget_layout(parent, *widgets):
    hbox = QtWidgets.QHBoxLayout(parent)
    for widget in widgets:
        hbox.addWidget(widget)

    return hbox


def create_vwidget_layout(parent, *widgets):
    vbox = QtWidgets.QVBoxLayout(parent)
    for widget in widgets:
        vbox.addWidget(widget)

    return vbox


def create_horizontal_layout(parent, *layouts):
    hbox = QtWidgets.QHBoxLayout(parent)
    for layout in layouts:
        hbox.addLayout(layout)

    return hbox


def create_vertical_layout(parent, *layouts):
    vbox = QtWidgets.QVBoxLayout(parent)
    for layout in layouts:
        vbox.addLayout(layout)

    return vbox


def ask(title, text):
    box = QtWidgets.QMessageBox()
    box.setWindowTitle(title)
    box.setText(text)
    box.setIcon(QtWidgets.QMessageBox.Question)
    box.addButton("Yes", QtWidgets.QMessageBox.YesRole)
    box.addButton("No", QtWidgets.QMessageBox.NoRole)
    return box.exec_() == 0


async def load_dialog(parent, file_filter, direc=None, more_paths=False):
    dialog = QtWidgets.QFileDialog(
        parent,
        "Open File",
        QtCore.QDir.homePath() if direc is None else str(direc),
        file_filter,
    )
    dialog.setFileMode(dialog.ExistingFiles)
    dialog.setAcceptMode(dialog.AcceptOpen)
    fut = asyncio.Future()

    def on_accept():
        files_path = dialog.selectedFiles()
        fut.set_result(
            (files_path if more_paths else files_path[0], dialog.selectedNameFilter())
        )

    dialog.accepted.connect(on_accept)
    dialog.rejected.connect(fut.cancel)
    dialog.open()
    return await fut


async def save_dialog(parent, file_filter, direc=None):

    dialog = QtWidgets.QFileDialog(
        parent,
        "Save File",
        QtCore.QDir.homePath() if direc is None else str(direc),
        file_filter,
    )
    dialog.setFileMode(dialog.ExistingFile)
    dialog.setAcceptMode(dialog.AcceptSave)
    fut = asyncio.Future()

    def on_accept():
        fut.set_result(dialog.selectedFiles()[0])

    dialog.accepted.connect(on_accept)
    dialog.rejected.connect(fut.cancel)
    dialog.open()
    return await fut
