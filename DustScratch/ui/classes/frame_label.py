# DustScratch - Film Stock Editor
# Copyright (C) 2018 Michele Valsesia

from PyQt5 import QtCore, QtGui, QtWidgets

from DustScratch.ui.util import convert_QImage_to_array


class FrameLabel(QtWidgets.QLabel):
    def __init__(self, parent=None):
        super().__init__(parent)

        self._mode = False
        self._already_paint = False
        self._zoom = 1
        self._od_w = 0
        self._od_h = 0
        self._width = 0
        self._height = 0
        self._pens = [8]
        self._paths = [QtGui.QPainterPath()]

    def setMode(self, mode):
        self._mode = mode
        if not self._mode:
            self.resetPath()
        else:
            self.update()

    def setZoom(self, zoom):
        self._zoom = zoom

    def setPen(self, dim):
        self._pens.append(dim)
        self._paths[-1].closeSubpath()
        self._paths.append(QtGui.QPainterPath())

    def setPixmap(self, input_pixmap):
        pixmap = input_pixmap.scaled(
            input_pixmap.width() * self._zoom,
            input_pixmap.height() * self._zoom,
            QtCore.Qt.KeepAspectRatio,
            QtCore.Qt.SmoothTransformation,
        )
        super().setPixmap(pixmap)

    def getPathImage(self):
        mask = QtGui.QImage(
            self.pixmap().width() / self._zoom,
            self.pixmap().height() / self._zoom,
            QtGui.QImage.Format_Grayscale8,
        )
        mask.fill(0)

        painter = QtGui.QPainter()
        painter.begin(mask)

        width = (self.width() / 2) - (self.pixmap().width() / 2)
        height = (self.height() / 2) - (self.pixmap().height() / 2)
        painter.translate(-width, -height)

        for pen, path in zip(self._pens, self._paths):
            painter.setPen(QtGui.QPen(QtCore.Qt.white, pen))
            painter.drawPath(path)

        painter.end()

        return convert_QImage_to_array(mask, mask.format())

    def resizeEvent(self, event):
        self._od_w = event.oldSize().width()
        self._od_h = event.oldSize().height()
        # self.setPen(4)
        super().resizeEvent(event)

    def paintEvent(self, event):
        painter = QtGui.QPainter(self)
        pix_w = self.pixmap().width()
        pix_h = self.pixmap().height()
        width = self.width() / 2 - pix_w / 2
        height = self.height() / 2 - pix_h / 2
        self._rect = QtCore.QRect(width, height, pix_w, pix_h)

        if self._zoom == 1:
            self._width = width
            self._height = height

        painter.drawPixmap(self._rect, self.pixmap())

        if self._zoom < 1:
            old_w = self.width() / 2 - ((pix_w / 2) * 1 / self._zoom)
            old_h = self.height() / 2 - ((pix_h / 2) * 1 / self._zoom)
            diff_w = ((1 / self._zoom) - 1) * old_w + width / self._zoom
            diff_h = ((1 / self._zoom) - 1) * old_h + height / self._zoom
            painter.translate(-old_w, -old_h)
            painter.scale(self._zoom, self._zoom)
            painter.translate(diff_w, diff_h)

        if self._zoom > 1:
            painter.scale(self._zoom, self._zoom)
            painter.translate(-self._width, -self._height)

        """ Drawing path on pixmap """
        for pen, path in zip(self._pens, self._paths):
            painter.setPen(QtGui.QPen(QtCore.Qt.white, pen))
            painter.drawPath(path)

    def mousePressEvent(self, event):
        if not self._mode:
            return None

        print("Mouse pos: ", event.pos())

        if self._zoom != 1 or not self._rect.contains(event.pos()):
            self._already_paint = False
            return None

        self._paths[-1].moveTo(event.pos())
        self._already_paint = True
        self.update()

    def mouseMoveEvent(self, event):
        if not self._mode:
            return None

        if (
            self._zoom != 1
            or not self._rect.contains(event.pos())
            or not self._already_paint
        ):
            return None

        self._paths[-1].lineTo(event.pos())
        self.update()

    def resetPath(self):
        self._paths = [QtGui.QPainterPath()]
        self._pens = [8]
        self.update()
