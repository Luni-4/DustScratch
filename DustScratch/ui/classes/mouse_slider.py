# DustScratch - Film Stock Editor
# Copyright (C) 2018 Michele Valsesia

from PyQt5 import QtCore, QtWidgets


class MouseSlider(QtWidgets.QSlider):
    def __init__(self, parent):
        super().__init__(parent)
        self._opt = QtWidgets.QStyleOptionSlider()

    def mousePressEvent(self, mouse_event):

        self.initStyleOption(self._opt)
        self.sr = self.style().subControlRect(
            QtWidgets.QStyle.CC_Slider,
            self._opt,
            QtWidgets.QStyle.SC_SliderHandle,
            self,
        )

        if (
            self.sr.contains(mouse_event.pos()) == False
            and mouse_event.buttons() == QtCore.Qt.LeftButton
        ):

            diff = self.maximum() - self.minimum()
            if self.orientation() == QtCore.Qt.Vertical:
                height = self.height()
                temp = (diff * (height - mouse_event.y())) / height
            else:
                temp = (diff * mouse_event.x()) / self.width()

            self.setValue(int(self.minimum() + temp))
            mouse_event.accept()
        else:
            super().mousePressEvent(mouse_event)
