# DustScratch - Film Stock Editor
# Copyright (C) 2018 Michele Valsesia

from PyQt5 import QtCore, QtWidgets

from DustScratch.ui.classes.mouse_slider import MouseSlider
from DustScratch.ui.util import create_hwidget_layout


class SliderSpinBox(QtWidgets.QWidget):
    def __init__(self, label_name, minimum, maximum, step, parent=None):
        super().__init__(parent)

        self.label = QtWidgets.QLabel(label_name, self)

        self.slider = MouseSlider(self)
        self.slider.setOrientation(QtCore.Qt.Horizontal)
        self.slider.setTracking(True)
        self.slider.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.slider.setMinimum(minimum)
        self.slider.setMaximum(maximum)
        self.slider.setSingleStep(step)

        self.spinbox = QtWidgets.QSpinBox(self)
        self.spinbox.setMinimum(minimum)
        self.spinbox.setMaximum(maximum)
        self.spinbox.setSingleStep(step)

        self._connect_ui_signals()

    def setProperty(self, prop_name):
        self.slider.setProperty("name", prop_name)
        self.spinbox.setProperty("name", prop_name)

    def getLayout(self):
        return create_hwidget_layout(None, self.label, self.slider, self.spinbox)

    def _connect_ui_signals(self):
        self.slider.valueChanged.connect(self._change_spinbox)
        self.spinbox.valueChanged.connect(self._change_slider)

    def _disconnect_ui_signals(self):
        self.slider.valueChanged.disconnect(self._change_spinbox)
        self.spinbox.valueChanged.disconnect(self._change_slider)

    def _change_spinbox(self):
        self._disconnect_ui_signals()
        self.spinbox.setValue(self.slider.value())
        self._connect_ui_signals()

    def _change_slider(self):
        self._disconnect_ui_signals()
        self.slider.setValue(self.spinbox.value())
        self._connect_ui_signals()
