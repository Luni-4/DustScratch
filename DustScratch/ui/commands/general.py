# DustScratch - Film Stock Editor
# Copyright (C) 2018 Michele Valsesia

from DustScratch.processor.commands import Command
from DustScratch.ui.dialogs.about import AboutDialog

""" General Commands """


class AboutCommand(Command):
    name = "about"

    async def run(self):
        AboutDialog(self.gui.main_window).exec_()


class UndoCommand(Command):
    name = "undo"

    async def run(self):
        self.gui.undo.undo()

    @property
    def is_enabled(self):
        return self.gui.undo.has_undo


class QuitCommand(Command):
    name = "quit"

    async def run(self):
        self.gui.quit()


COMMANDS = [AboutCommand, UndoCommand, QuitCommand]
