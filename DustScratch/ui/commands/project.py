# DustScratch - Film Stock Editor
# Copyright (C) 2018 Michele Valsesia

from PyQt5 import QtCore, QtWidgets

from DustScratch.processor.commands import Command
from DustScratch.ui.util import load_dialog, save_dialog

VIDEO_LOAD_FILTER = """ Videos (*.mp4 *.mkv *.avi *.mov);;
                        Images (*.png *.tiff *.gif *.jpg *.dpx *.exr);;
                        Sequence (*.png *.dpx);;
                        All Files (*.*)"""

PROJECT_FILE_FILTER = "DustScratch (*.ds)"


def _ask_about_unsaved_changes(gui):
    if not gui.project.needs_save:
        return True

    return DustScratch.ui.util.ask(
        "Save Changes",
        "There are unsaved changes. "
        "Are you sure you want to close the current file?",
    )


class NewProjectCommand(Command):
    name = "new_project"

    async def run(self):

        if not _ask_about_unsaved_changes(self.gui):
            return None

        path, filters = await self.gui.exec(
            load_dialog, file_filter=VIDEO_LOAD_FILTER, more_paths=True
        )

        if path is None:
            return None

        seq = False
        if filters.split()[0] == "Sequence":
            seq = True

        self.gui.project.new_project()
        self.gui.project.loaded_video_path = path

        self.gui.filters.clean_filters()
        self.gui.video.close_video()

        self.gui.video.open_video(path, seq)
        self.gui.video.create_filtered_clips()


class OpenProjectCommand(Command):
    name = "open_project"

    async def run(self):
        if not _ask_about_unsaved_changes(self.gui):
            return None

        path, _ = await self.gui.exec(load_dialog, file_filter=PROJECT_FILE_FILTER)

        if path is None:
            return None

        self.gui.filters.clean_filters()
        self.gui.video.close_video()

        self.gui.project.load_project(path)
        self.gui.video.open_video(self.gui.project.loaded_video_path)
        self.gui.video.create_filtered_clips()
        self.gui.project.run_saved_project()


class SaveProjectCommand(Command):
    name = "save_project"

    async def run(self):

        path = await self.gui.exec(save_dialog, file_filter=PROJECT_FILE_FILTER)

        if path is None:
            return None

        self.gui.project.save_project(path)

    @property
    def is_enabled(self):
        return self.gui.project.is_open


COMMANDS = [NewProjectCommand, OpenProjectCommand, SaveProjectCommand]
