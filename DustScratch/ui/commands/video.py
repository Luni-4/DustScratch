# DustScratch - Film Stock Editor
# Copyright (C) 2018 Michele Valsesia

from PyQt5 import QtCore, QtWidgets

from DustScratch.processor.commands import Command
from DustScratch.ui.util import save_dialog

VIDEO_SAVE_FILTER = "Videos (*.avi);;"
IMAGE_SAVE_FILTER = "Images (*.png *.dpx);;"


class SaveVideoCommand(Command):
    name = "save_video"

    async def run(self):
        path = await self.gui.exec(save_dialog, file_filter=VIDEO_SAVE_FILTER)
        if path is None:
            return None

        self.gui.video.save_video(path)

    @property
    def is_enabled(self):
        return self.gui.project.is_open


class SaveFrameSequenceCommand(Command):
    name = "save_frame_sequence"

    async def run(self):
        path = await self.gui.exec(save_dialog, file_filter=IMAGE_SAVE_FILTER)

        if path is None:
            return None

        self.gui.video.save_frames(path)

    @property
    def is_enabled(self):
        return self.gui.project.is_open


class SaveFrameCommand(Command):
    name = "save_frame"

    async def run(self):
        path = await self.gui.exec(save_dialog, file_filter=IMAGE_SAVE_FILTER)

        if path is None:
            return None

        self.gui.video.save_frame(path)

    @property
    def is_enabled(self):
        return self.gui.project.is_open


COMMANDS = [SaveVideoCommand, SaveFrameSequenceCommand, SaveFrameCommand]
