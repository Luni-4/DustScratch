# DustScratch - Film Stock Editor
# Copyright (C) 2018 Michele Valsesia

from DustScratch.processor.commands import Command


class ChangeFrameViewCommand(Command):
    name = "change_frame_view"

    async def run(self):
        self.gui.video.change_frame_view()

    @property
    def is_enabled(self):
        return self.gui.project.is_open


class ZoomIn(Command):
    name = "zoom_in"

    async def run(self):
        self.gui.video.zoom_in()

    @property
    def is_enabled(self):
        return self.gui.project.is_open


class ZoomOut(Command):
    name = "zoom_out"

    async def run(self):
        self.gui.video.zoom_out()

    @property
    def is_enabled(self):
        return self.gui.project.is_open


class StartManualModeCommand(Command):
    name = "start_manual_mode"

    async def run(self):
        if self.gui.video.is_manual:
            return None

        self.gui.video.set_manual(True)

    @property
    def is_enabled(self):
        return self.gui.project.is_open


class EndManualModeCommand(Command):
    name = "end_manual_mode"

    async def run(self):
        if not self.gui.video.is_manual:
            return None

        self.gui.video.set_manual(False)

    @property
    def is_enabled(self):
        return self.gui.project.is_open


class LogCommand(Command):
    name = "log"

    async def run(self):
        self.gui.main_window.log.toggleViewAction().activate(0)


COMMANDS = [
    ChangeFrameViewCommand,
    ZoomIn,
    ZoomOut,
    StartManualModeCommand,
    EndManualModeCommand,
    LogCommand,
]
