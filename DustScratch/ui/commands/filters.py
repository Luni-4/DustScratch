# DustScratch - Film Stock Editor
# Copyright (C) 2018 Michele Valsesia

from DustScratch.processor.commands import Command
from DustScratch.ui.dialogs.filters.dust_remover import DustRemoverDialog


class DustRemoverCommand(Command):
    name = "dust_remover_filter"

    async def run(self):
        DustRemoverDialog(self.gui, self.name, self.gui.main_window).exec_()

    @property
    def is_enabled(self):
        return self.gui.project.is_open


class InpaintCommand(Command):
    name = "inpaint_filter"

    async def run(self):
        if not self.gui.video.is_manual:
            return None

        self.gui.filters.create_filter(InpaintCommand.name)
        self.gui.filters.run_filter()

    @property
    def is_enabled(self):
        return self.gui.project.is_open


COMMANDS = [DustRemoverCommand, InpaintCommand]
