# DustScratch - Film Stock Editor
# Copyright (C) 2018 Michele Valsesia

from PyQt5 import QtCore, QtWidgets

from DustScratch.ui.classes.slider_spinbox import SliderSpinBox
from DustScratch.ui.util import (
    create_double_spinbox,
    create_grid_groupbox,
    create_hwidget_layout,
    create_radio_button_group,
    create_vertical_layout,
    create_vwidget_layout,
)


class _MotionBox(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.dfactor_spinbox, h_dfactor = create_double_spinbox(
            label_name="Dfactor", minimum=0, maximum=10, step=0.1, parent=self
        )
        self.dfactor_spinbox.setProperty("name", "dfactor")

        self.group = create_grid_groupbox("Motion", self, (h_dfactor, 1, 0))

        vbox = create_vwidget_layout(self, self.group)


class _NoiseBox(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.noise = SliderSpinBox("Noise", -1, 64, 1, self)
        self.noise.setProperty("noise")

        self.noisy = SliderSpinBox("Noisy", -1, 64, 1, self)
        self.noisy.setProperty("noisy")

        self.group = create_grid_groupbox(
            "Noise",
            self,
            (self.noise.getLayout(), 1, 0),
            (self.noisy.getLayout(), 2, 0),
        )

        vbox = create_vwidget_layout(self, self.group)


class _RestoreBox(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.repmode = SliderSpinBox("Repmode", 0, 24, 1, self)
        self.repmode.setProperty("repmode")

        self.dist = SliderSpinBox("Dist", 1, 5, 1, self)
        self.dist.setProperty("dist")

        self.gmt = SliderSpinBox("Gmthreshold", 0, 100, 1, self)
        self.gmt.setProperty("gmthreshold")

        self.tolerance = SliderSpinBox("Tolerance", 0, 50, 1, self)
        self.tolerance.setProperty("tolerance")

        self.pths = SliderSpinBox("Pthreshold", 0, 100, 1, self)
        self.pths.setProperty("pthreshold")

        self.cths = SliderSpinBox("Cthreshold", 0, 100, 1, self)
        self.cths.setProperty("cthreshold")

        self.dmode_button_group, h_dmode = create_radio_button_group(
            "Dmode", self, ("0", 0), ("1", 1), ("2", 2)
        )
        self.dmode_button_group.setProperty("name", "dmode")

        self.group = create_grid_groupbox(
            "Restore",
            self,
            (self.repmode.getLayout(), 1, 0),
            (self.dist.getLayout(), 2, 0),
            (self.gmt.getLayout(), 3, 0),
            (self.tolerance.getLayout(), 4, 0),
            (self.pths.getLayout(), 5, 0),
            (self.cths.getLayout(), 6, 0),
            (h_dmode, 7, 0),
        )

        vbox = create_vwidget_layout(self, self.group)


class DustRemoverDialog(QtWidgets.QDialog):
    def __init__(self, gui, name, parent=None):
        super().__init__(parent)
        self._gui = gui
        self._name = name

        self.setWindowTitle("Dust Remover Filter")

        help_flag = QtCore.Qt.WindowContextHelpButtonHint
        self.setWindowFlags(self.windowFlags() & ~help_flag)

        self.group_one = _MotionBox(self)
        self.group_two = _NoiseBox(self)
        self.group_three = _RestoreBox(self)

        self.grey = QtWidgets.QCheckBox("Grey", self)
        self.grey.setProperty("name", "grey")

        self.checkbox = QtWidgets.QCheckBox("All Frames", self)
        self.checkbox.setChecked(True)

        self.group_four = create_grid_groupbox(
            "Options",
            None,
            (create_vwidget_layout(None, self.grey, self.checkbox), 1, 0),
        )

        grid = QtWidgets.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        grid.addWidget(self.group_one, 1, 0)
        grid.addWidget(self.group_two, 1, 1)
        grid.addWidget(self.group_three, 2, 0)
        grid.addWidget(self.group_four, 2, 1)

        self.reset = QtWidgets.QPushButton("Reset", self)
        self.reset.setAutoDefault(False)
        self.apply = QtWidgets.QPushButton("Apply", self)
        self.apply.setAutoDefault(False)
        self.undo = QtWidgets.QPushButton("Undo", self)

        h_buttons = create_hwidget_layout(None, self.reset, self.apply, self.undo)

        vbox = create_vertical_layout(self, grid, h_buttons)

        self._widgets = self.findChildren(
            (
                QtWidgets.QSpinBox,
                QtWidgets.QDoubleSpinBox,
                QtWidgets.QButtonGroup,
                QtWidgets.QCheckBox,
            )
        )

        self._widgets.pop()

        gui.filters.create_filter(name)

        self._set_params(gui.filters.check_params(name))

        self._connect_ui_signals()

    def _connect_ui_signals(self):
        self.reset.clicked.connect(self._reset_values)
        self.apply.clicked.connect(self._send_values)
        self.undo.clicked.connect(self.reject)

    def _disconnect_ui_signals(self):
        self.reset.clicked.disconnect(self._reset_values)
        self.apply.clicked.disconnect(self._send_values)
        self.undo.clicked.disconnect(self.reject)

    def _reset_values(self):
        self._disconnect_ui_signals()
        default_params = self._gui.filters.get_default_params(self._name)
        self._set_params(default_params)
        self.checkbox.setChecked(True)
        self._connect_ui_signals()

    def _set_params(self, filter_params):
        for widget in self._widgets[:-1]:
            prop = widget.property("name")
            if isinstance(widget, QtWidgets.QButtonGroup):
                widget.button(filter_params[prop]).setChecked(True)
            else:
                widget.setValue(filter_params[prop])
        widget = self._widgets[-1]
        widget.setChecked(bool(filter_params[widget.property("name")]))

    def _save_params(self):
        params = {}
        for widget in self._widgets[:-1]:
            if isinstance(widget, QtWidgets.QButtonGroup):
                params[widget.property("name")] = widget.checkedId()
            else:
                params[widget.property("name")] = widget.value()
        widget = self._widgets[-1]
        params[widget.property("name")] = int(widget.isChecked())

        self._gui.filters.save_current_params(self._name, params)

    def _send_values(self):
        self._disconnect_ui_signals()
        self._save_params()
        self._gui.filters.set_is_all(self.checkbox.isChecked())
        self._gui.filters.read_params(self._name)
        self._gui.filters.run_filter()
        self.accept()
