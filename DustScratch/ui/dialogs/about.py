# DustScratch - Film Stock Editor
# Copyright (C) 2018 Michele Valsesia

from PyQt5 import QtCore, QtGui, QtWidgets

_FILENAME = "DustScratch/data/mips.png"


class AboutDialog(QtWidgets.QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.setWindowTitle("About")

        help_flag = QtCore.Qt.WindowContextHelpButtonHint
        self.setWindowFlags(self.windowFlags() & ~help_flag)

        self._logo_image = QtGui.QImage(
            QtCore.QDir.currentPath() + QtCore.QDir.separator() + _FILENAME
        )

        self._logo_image = self._logo_image.scaled(476, 387, QtCore.Qt.KeepAspectRatio)
        self._logo_image.convertToFormat(QtGui.QImage.Format_RGB32)

        self._logo_label = QtWidgets.QLabel(self)
        self._logo_label.setAlignment(QtCore.Qt.AlignCenter)
        self._logo_label.setStyleSheet("background-color:black;")
        self._logo_label.setPixmap(QtGui.QPixmap.fromImage(self._logo_image))

        self._information = QtWidgets.QLabel(
            "Copyright (C) 2018 Michele Valsesia", self
        )
        self._information.setAlignment(QtCore.Qt.AlignCenter)

        vbox = QtWidgets.QVBoxLayout(self)
        vbox.setSpacing(2)
        vbox.setContentsMargins(0, 0, 0, 0)
        vbox.addWidget(self._logo_label)
        vbox.addWidget(self._information)
