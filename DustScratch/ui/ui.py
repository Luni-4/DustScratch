# DustScratch - Film Stock Editor
# Copyright (C) 2018 Michele Valsesia

import argparse
import asyncio
import sys

import quamash
from PyQt5 import QtCore, QtWidgets

import DustScratch.ui.gui
import DustScratch.ui.main_window


def run():
    QtCore.pyqtRemoveInputHook()
    app = QtWidgets.QApplication(sys.argv + ["--name", "DustScratch"])
    app.setApplicationName("DustScratch")
    loop = quamash.QEventLoop(app)
    asyncio.set_event_loop(loop)

    gui = DustScratch.ui.gui.Gui()

    main_window = DustScratch.ui.main_window.MainWindow(gui)
    gui.set_main_window(main_window)

    main_window.show()

    with loop:
        loop.run_forever()
