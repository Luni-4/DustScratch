# DustScratch - Film Stock Editor
# Copyright (C) 2018 Michele Valsesia

import datetime
import enum
import re

from PyQt5 import QtCore, QtGui, QtWidgets


class Color(enum.Enum):
    Black = QtGui.QColor(0, 0, 0)
    Green = QtGui.QColor(77, 162, 88)
    Grey = QtGui.QColor(127, 127, 127)
    Red = QtGui.QColor(255, 0, 0)


class ConsoleSyntaxHighlight(QtGui.QSyntaxHighlighter):
    def __init__(self, parent):
        super().__init__(parent)

        self._font = QtGui.QFontDatabase.systemFont(QtGui.QFontDatabase.FixedFont)

        self._style_map = {}

        self._regex = re.compile(
            r"^"
            r"(?P<timestamp>\[[^\]]+\]) "
            r"(?P<prefix>(?P<log_level>error|warning|info): )"
            r"(?P<text>.*)"
            r"$"
        )

        self.update_style_map()

    def get_font(self):
        return self._font

    def set_font(self, font):
        self._font = font
        self.update_style_map()

    def update_style_map(self):
        self._style_map = {
            "error": self._get_format(Color.Red),
            "warning": self._get_format(Color.Green),
            "info": self._get_format(Color.Black),
            "timestamp": self._get_format(Color.Grey),
        }

        QtWidgets.QApplication.setOverrideCursor(QtGui.QCursor(QtCore.Qt.WaitCursor))
        self.rehighlight()
        QtWidgets.QApplication.restoreOverrideCursor()

    def highlightBlock(self, msg):
        for match in re.finditer(self._regex, msg):
            start = match.start()
            start_of_prefix = match.start() + len(match.group("timestamp"))
            start_of_text = start_of_prefix + len(match.group("prefix"))
            end = match.end()

            self.setFormat(start, start_of_prefix - start, self._style_map["timestamp"])
            self.setFormat(
                start_of_prefix,
                start_of_text - start_of_prefix,
                self._style_map[match.group("log_level")],
            )
            self.setFormat(
                start_of_text,
                end - start,
                self._style_map[match.group("log_level")],
            )

            def _get_format(self, color):
                fmt = QtGui.QTextCharFormat()

        fmt.setForeground(QtGui.QBrush(color.value))
        fmt.setFont(self._font)
        return fmt


class ConsoleLogWindow(QtWidgets.QTextEdit):
    scroll_lock_changed = QtCore.pyqtSignal()

    def __init__(self, gui, parent):
        super().__init__(parent)
        self._empty = True

        self._syntax_highlight = ConsoleSyntaxHighlight(self)
        self.setObjectName("ConsoleLog")
        self.setReadOnly(True)
        self.setLineWrapMode(QtWidgets.QTextEdit.NoWrap)

        gui.log.logged.connect(self._print_log)

    def changeEvent(self, event):
        self._syntax_highlight.update_style_map()

    def contextMenuEvent(self, event):
        console_menu = QtWidgets.QMenu()
        console_menu.addAction(
            "Clear Console", lambda: self.document().setPlainText("")
        )
        console_menu.exec(event.globalPos())

    def _print_log(self, level, msg):

        separator = "" if self._empty else "\n"

        self.moveCursor(QtGui.QTextCursor.End)
        cursor = QtGui.QTextCursor(self.textCursor())
        cursor.insertText(
            f"{separator}"
            f"[{datetime.datetime.now():%H:%M:%S.%f}] "
            f"{level.name.lower()}: "
            f"{msg}"
        )

        self.horizontalScrollBar().setValue(self.verticalScrollBar().minimum())
        self.verticalScrollBar().setValue(self.verticalScrollBar().maximum())
        self._empty = False


class Console(QtWidgets.QWidget):
    def __init__(self, gui, parent):
        super().__init__(parent)

        self.log_window = ConsoleLogWindow(gui, self)

        layout = QtWidgets.QVBoxLayout(self)
        layout.setSpacing(4)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(self.log_window)
