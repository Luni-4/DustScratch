# DustScratch - Film Stock Editor
# Copyright (C) 2018 Michele Valsesia

from PyQt5 import QtCore, QtGui, QtWidgets

import DustScratch.ui.console
import DustScratch.ui.menu
import DustScratch.ui.shortcuts
import DustScratch.ui.statusbar
import DustScratch.ui.util
import DustScratch.ui.video
from DustScratch.processor.shortcuts import ShortcutContext


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, gui):

        super().__init__()
        self._gui = gui

        gui.project.loaded.connect(self._update_title)
        gui.quit_requested.connect(self.close)

        self.video = DustScratch.ui.video.Video(gui, self)
        self.status_bar = DustScratch.ui.statusbar.StatusBar(gui, self)
        self.console = DustScratch.ui.console.Console(gui, self)

        self.log = QtWidgets.QDockWidget(self)
        self.log.setAllowedAreas(QtCore.Qt.BottomDockWidgetArea)
        self.log.setWidget(self.console)
        self.log.close()

        self._update_title()
        self._create_menu()
        self._create_shortcuts()
        self._create_ui()

    def _update_title(self):
        self.setWindowTitle(
            f"DustScratch - {self._gui.project.path}"
            if self._gui.project.path
            else "DustScratch"
        )

    def _create_menu(self):
        return DustScratch.ui.menu.create_menu_function(
            self._gui, self.menuBar(), ShortcutContext.Global
        )

    def _create_shortcuts(self):
        DustScratch.ui.shortcuts.create_shortcuts(
            self._gui,
            {DustScratch.ui.shortcuts.ShortcutContext.Global: self},
            self._gui.shortcuts.shortcuts,
        )

    def _create_ui(self):
        self.setCentralWidget(self.video)
        self.addDockWidget(QtCore.Qt.BottomDockWidgetArea, self.log)
        self.setStatusBar(self.status_bar)

    def closeEvent(self, event):
        if self._gui.project.needs_save and not DustScratch.ui.util.ask(
            "Save Changes",
            "There are unsaved changes. " "Are you sure you want to exit the program?",
        ):
            event.ignore()
        else:
            self._gui.video.shutdown_video()
            event.accept()
