# DustScratch - Film Stock Editor
# Copyright (C) 2018 Michele Valsesia

import functools

from PyQt5 import QtWidgets


class MenuItem:
    pass


class MenuSeparator(MenuItem):
    pass


class MenuFunction(MenuItem):
    def __init__(self, name, cname):
        self.name = name
        self.cname = cname


class SubMenu(MenuItem):
    def __init__(self, name, children):
        self.name = name
        self.children = children


MAIN_MENU = [
    SubMenu(
        "&File",
        [
            MenuFunction("&New Project", "new_project"),
            MenuFunction("&Open Project", "open_project"),
            MenuFunction("&Save Project", "save_project"),
            MenuSeparator(),
            MenuFunction("&Save Video", "save_video"),
            MenuSeparator(),
            MenuFunction("&Save Frame Sequence", "save_frame_sequence"),
            MenuFunction("&Save Frame", "save_frame"),
            MenuSeparator(),
            MenuFunction("&Quit", "quit"),
        ],
    ),
    SubMenu(
        "&Edit",
        [
            MenuFunction("Undo", "undo"),
            MenuSeparator(),
            MenuFunction("Start Manual Mode", "start_manual_mode"),
            MenuFunction("End Manual Mode", "end_manual_mode"),
            MenuSeparator(),
            MenuFunction("Zoom In", "zoom_in"),
            MenuFunction("Zoom Out", "zoom_out"),
        ],
    ),
    SubMenu(
        "&Filters",
        [
            MenuFunction("DustRemover", "dust_remover_filter"),
            MenuFunction("Inpaint", "inpaint_filter"),
        ],
    ),
    SubMenu(
        "&About",
        [MenuFunction("Log", "log"), MenuSeparator(), MenuFunction("About", "about")],
    ),
]


def _menu_about_to_show(menu):
    for action in menu.actions():
        if getattr(action, "command", None):
            action.setEnabled(action.command.is_enabled)


class _CommandAction(QtWidgets.QAction):
    def __init__(self, gui, item, parent):

        super().__init__(parent)
        self._gui = gui
        self.command = gui.command.get_command(item.cname)

        self.triggered.connect(self._run_command)
        self.setText(item.name)

    def _run_command(self):
        self._gui.command.run_command(self.command)


def _build_shortcut_map(gui):
    ret = {}
    for context, shortcuts in gui.shortcuts.shortcuts.items():
        for shortcut in shortcuts:
            ret[context, shortcut.command] = shortcut.shortcut
    return ret


def create_menu_function(
    gui, parent, shortcut_context, menu=MAIN_MENU, shortcut_map=None
):
    if shortcut_map is None:
        shortcut_map = _build_shortcut_map(gui)

    if hasattr(parent, "aboutToShow"):
        parent.aboutToShow.connect(functools.partial(_menu_about_to_show, parent))

    for item in menu:
        if isinstance(item, MenuSeparator):
            parent.addSeparator()
        elif isinstance(item, SubMenu):
            submenu = parent.addMenu(item.name)
            create_menu_function(
                gui, submenu, shortcut_context, item.children, shortcut_map
            )
        else:
            action = _CommandAction(gui, item, parent)
            shortcut = shortcut_map.get((shortcut_context, item.cname))
            if shortcut is not None:
                action.setText(action.text() + "\t" + shortcut)

            parent.addAction(action)
