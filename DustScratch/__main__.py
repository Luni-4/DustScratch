#!/usr/bin/env python3

# DustScratch - Film Stock Editor
# Copyright (C) 2018 Michele Valsesia

"""CLI endpoint."""


import argparse

import DustScratch.ui


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("file", nargs="?")
    return parser.parse_args()


def main():
    """CLI endpoint."""
    args = parse_args()

    DustScratch.ui.run()


if __name__ == "__main__":
    main()
