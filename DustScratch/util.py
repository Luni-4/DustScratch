# DustScratch - Film Stock Editor
# Copyright (C) 2018 Michele Valsesia

import importlib
import pathlib
import pickle
import time
import zlib


def load_commands(path, list_name):
    specs = []
    path = pathlib.Path(path)
    subpaths = filter(lambda s: s.stem != "__init__", path.glob("*.py"))
    for subpath in subpaths:
        spec = importlib.util.spec_from_file_location(
            ".".join([*path.parts] + [subpath.relative_to(path).stem]), str(subpath)
        )
        specs.append(spec)

    for spec in specs:
        mod = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(mod)
        for cls in getattr(mod, list_name):
            yield cls


def serialize_compress(data):
    return zlib.compress(pickle.dumps(data, protocol=pickle.HIGHEST_PROTOCOL))


def unserialize_uncompress(data):
    return pickle.loads(zlib.decompress(data))


def split_parent_extension(path):
    parent = pathlib.Path(path).resolve().parent
    stem = pathlib.Path(path).resolve().stem
    parent = parent / stem
    extension = pathlib.Path(path).resolve().suffix
    return (str(parent), str(extension).lower())


def add_extension(path, ext):
    filePath, fileExt = split_parent_extension(path)
    if not fileExt:
        return filePath + "." + ext
    return path


def timeit(method):
    def timed(*args, **kw):
        print("Method name: ", method.__name__)
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        print("Elapsed time:", te - ts, "seconds")
        return result

    return timed


def merge_dicts(dictionary):
    joined = {}
    for value in dictionary.values():
        joined.update(value)
    return joined
