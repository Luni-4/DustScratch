# DustScratch - Film Stock Editor
# Copyright (C) 2018 Michele Valsesia

from DustScratch.api.dustRemover import dustRemover
from DustScratch.processor.filters import Filter


class DustRemover(Filter):
    name = "dust_remover_filter"

    default_params = dustRemover.default

    def __init__(self, gui):
        super().__init__(gui)
        self._instance = dustRemover()

    def set_params(self, params):
        super().set_params(params)

        """ Setting the clip to filter """
        self._instance.setClip(self.gui.filters.choose_clip())

        """ Setting filter parameters """
        self._instance.setDfactor(self.params["dfactor"])
        self._instance.setNoise(self.params["noise"])
        self._instance.setNoisy(self.params["noisy"])
        self._instance.setRepmode(self.params["repmode"])
        self._instance.setGmt(self.params["gmthreshold"])
        self._instance.setDist(self.params["dist"])
        self._instance.setTolerance(self.params["tolerance"])
        self._instance.setPth(self.params["pthreshold"])
        self._instance.setCth(self.params["cthreshold"])
        self._instance.setDmode(self.params["dmode"])
        self._instance.setGrey(self.params["grey"])

    def run(self):

        """ Filter the clip """
        clip_filtered = self._instance.filter()

        """ Save the entire clip or just a frame """
        self.gui.filters.choose_save_mode(clip_filtered)


FILTERS = [DustRemover]
