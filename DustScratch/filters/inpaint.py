# DustScratch - Film Stock Editor
# Copyright (C) 2018 Michele Valsesia

from DustScratch.api.image import inpainting
from DustScratch.processor.filters import Filter


class Inpaint(Filter):
    name = "inpaint_filter"

    default_params = {"radius": 2}

    def __init__(self, gui):
        super().__init__(gui)
        self._mask = gui.video.mask

    def set_mask(self, mask):
        self._mask = mask

    def run(self):
        frame = inpainting(
            self.gui.video.get_plain_frame(self.gui.video.frame_idx), self._mask, 2
        )

        clip_filtered = self.gui.video.convert_to_clip(frame)

        self.gui.filters.choose_save_mode(clip_filtered)


FILTERS = [Inpaint]
