# DustScratch - Film Stock Editor
# Copyright (C) 2018 Michele Valsesia

import cv2

from DustScratch.api.video import Video


class VideoWriter(Video):
    def __init__(self):
        super().__init__()

    def openVideo(self, path, fourcc, fps, width, height):
        self._v = cv2.VideoWriter(
            path,
            cv2.CAP_FFMPEG,
            cv2.VideoWriter_fourcc(*fourcc),
            fps,
            (width, height),
            True,
        )

        if self._v is None:
            return False

        self._width = width
        self._height = height
        self._fps = fps
        self._open = True
        return True

    def closeVideo(self):
        super().closeVideo()
        self._v.release()
        self._v = None

    def writeFrame(self, frame):
        if not self.isOpen:
            return None

        framew = self.changeFrameColorspace(frame, "BGR-RGB")

        self._v.write(framew)
        self._frame_count += 1
