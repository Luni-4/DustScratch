# DustScratch - Film Stock Editor
# Copyright (C) 2018 Michele Valsesia

import functools

import cv2
import numpy as np
import rawpy
import vapoursynth as vs

import DustScratch.util

colorspaceMapper = {
    "BGR-RGB": cv2.COLOR_BGR2RGB,
    "RGB-BGR": cv2.COLOR_RGB2BGR,
    "BGR-GRAY": cv2.COLOR_BGR2GRAY,
    "GRAY-BGR": cv2.COLOR_GRAY2BGR,
    "BGRA-BGR": cv2.COLOR_BGRA2BGR,
}

_RAWPY_IMAGES = [".dng", ".ari"]
_VS_IMAGES = [".psd", ".jp2", ".jxr"]


def VSFrameToImage(frame, num_planes):
    planes = []
    for plane in range(num_planes):
        planes.append(np.array(frame.get_read_array(plane)))

    return np.stack(planes, axis=2)


def ImageToVSClip(image, width, height):
    def frame(n, f, img):
        fout = f.copy()
        for plane in range(f.format.num_planes):
            ptr = fout.get_write_array(plane)
            ptr[:, :] = np.ascontiguousarray(img[:, :, plane])
        return fout

    if isinstance(image, vs.VideoNode):
        return image

    core = vs.core
    clip = core.std.BlankClip(
        width=width, height=height, format=vs.RGB24, length=1, fpsnum=1, fpsden=1
    )

    return core.std.ModifyFrame(
        clip=clip, clips=clip, selector=functools.partial(frame, img=image)
    )


"""Image functions"""


def openImage(path):
    _, fileExt = DustScratch.util.split_parent_extension(path)
    out = None

    if fileExt in _VS_IMAGES:
        core = vs.core
        image = core.imwri.Read(path)
        out = VSFrameToImage(image.get_frame(0), image.format.num_planes)
    elif fileExt in _RAWPY_IMAGES:
        with rawpy.imread(path) as raw:
            out = raw.postprocess()
    else:
        out = cv2.imread(path, cv2.IMREAD_UNCHANGED)

    return out


def writeImage(img, path):
    cv2.imwrite(path, img)


def getWidth(img):
    return img.shape[1]


def getHeight(img):
    return img.shape[0]


def getDepth(img):
    return img.shape[2]


def getBps(img):
    return img.dtype.itemsize * 8


def countPixels(img):
    return img.size


def countNonZeroPixels(img):
    return cv2.countNonZero(img)


def convertColorspace(img, colorspace):
    color = colorspaceMapper[colorspace]
    if color is None:
        return img
    return cv2.cvtColor(img, color)


def convertTo8(img):
    if img.dtype in ["uint16", "uint32"]:
        return cv2.convertScaleAbs(img, alpha=1 / 256, beta=0)
    elif img.dtype == "float32":
        return cv2.convertScaleAbs(img, alpha=255, beta=0)
    else:
        return img


def absDiff(img1, img2):
    return cv2.absdiff(img1, img2)


def thresholdGray(img, tsh=127, value=255):
    _, imgt = cv2.threshold(img, tsh, value, cv2.THRESH_BINARY)

    return imgt


"""Image Filtering"""


def dilate(img, kernel, iterations=1):
    return cv2.dilate(img, kernel, iterations)


def inpainting(img, kernel, radius=2):

    if getDepth(img) == 4:
        img = convertColorspace(img, "BGRA-BGR")

    return cv2.inpaint(img, kernel, radius, cv2.INPAINT_TELEA)


def sobel(img, ddepth=1, dx=1, dy=1, ksize=5):
    return cv2.Sobel(img, cv2.CV_8U, ddepth, dx, dy, ksize)


def gaussianBlur(img, kernel=(5, 5), sigmax=0, sigmay=0):
    return cv2.GaussianBlur(img, kernel, sigmax, sigmay)
