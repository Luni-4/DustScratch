# DustScratch - Film Stock Editor
# Copyright (C) 2018 Michele Valsesia

import abc

import vapoursynth as vs

from DustScratch.api.videoReader import VideoReader


def createMethod(target, name):
    attr = target.setter[name]

    def method(val):
        target.setParam(attr, val)

    return method


class Filter(abc.ABC):
    def __init__(self):
        self._params = {}
        for name in self.setter.keys():
            setattr(self, name, createMethod(self, name))

    @classmethod
    @property
    def name(cls):
        raise NotImplementedError("Filter has no name")

    @classmethod
    @property
    def default(cls):
        raise NotImplementedError("Filter has no default parameters")

    @classmethod
    @property
    def setter(cls):
        raise NotImplementedError("Filter has no setter")

    def setParam(self, name, value):
        self._params[name] = value

    def getParam(self, name):
        if name not in self.default.keys():
            return TypeError("Invalid filter parameter")

        obj = self._params.get(name, None)

        if obj is None:
            return self.default.get(name, None)

        return obj

    @abc.abstractmethod
    def filter(self):
        raise NotImplementedError("Filter has no implementation")


class VSFilter(Filter):
    def __init__(self):
        super().__init__()
        self.core = vs.core

    def setClip(self, clip):
        self.clipToFilter = clip.clip

    def videoOutput(self, clip):
        clip = self.core.resize.Bicubic(
            clip=clip,
            width=self.clipToFilter.width,
            height=self.clipToFilter.height,
            format=vs.RGB24,
        )
        vi = VideoReader()
        vi.replaceClip(clip)
        return vi
