# DustScratch - Film Stock Editor
# Copyright (C) 2018 Michele Valsesia

import DustScratch.api.image


class Video:
    def __init__(self):
        self._clean_video()
        self._v = None

    def openVideo(self):
        raise NotImplementedError("Video has no open video command")

    def closeVideo(self):
        if not self.isOpen:
            return

        self._clean_video()

    def changeFrameColorspace(self, frame, colorspace):
        return DustScratch.api.image.convertColorspace(frame, colorspace)

    def convertTo8(self, frame):
        return DustScratch.api.image.convertTo8(frame)

    @property
    def width(self):
        return self._width

    @property
    def height(self):
        return self._height

    @property
    def fps(self):
        return self._fps

    @property
    def frameCount(self):
        return self._frame_count

    @property
    def isOpen(self):
        return self._open

    def _clean_video(self):
        self._width = 0
        self._height = 0
        self._frame_count = 0
        self._fps = 0
        self._open = False
