# DustScratch - Film Stock Editor
# Copyright (C) 2018 Michele Valsesia

import numpy as np
import vapoursynth as vs

import DustScratch.api.image
from DustScratch.api.video import Video


class VideoReader(Video):
    def __init__(self):
        super().__init__()
        self._v = None
        self._frame_idx = 0
        vs.core.max_cache_size = 512

    def openVideo(self, path, seq=False):

        if not seq:
            self._v = DustScratch.api.image.openImage(path)

        if self._v is not None:
            self._frame_count = 1
            self._width = DustScratch.api.image.getWidth(self._v)
            self._height = DustScratch.api.image.getHeight(self._v)
            self._v = self.makeClip(self._v, self._width, self._height)

            self._open = True
            return self._open

        core = vs.core

        if seq:
            self._v = self._videoFromFrames(path)
        else:
            self._v = core.ffms2.Source(source=path)

        if self._v is None:
            return False

        self._set_video_params()

        if self._v.format.color_family != vs.ColorFamily.RGB:
            self._v = core.resize.Bicubic(
                clip=self._v,
                width=self._width,
                height=self._height,
                format=vs.RGB24,
                matrix_in_s="709",
            )
        else:
            self._v = core.resize.Bicubic(self._v, format=vs.RGB24)

        self._open = True
        return self._open

    def saveFrame(self, path, frame):
        frame_bgr = self.changeFrameColorspace(frame, "RGB-BGR")
        DustScratch.api.image.writeImage(frame_bgr, path)

    def closeVideo(self):
        super().closeVideo()
        self._v = None
        self._frame_idx = 0

    def getFrame(self, value):
        if not self.isOpen:
            return

        if self._frame_idx != value:
            self._frame_idx = max(0, min(value, self._frame_count))

        frame = self._v.get_frame(self._frame_idx)

        return frame

    def clipFrame(self, idx):
        if idx > self.frameCount - 1:
            idx = 0
        return self._v[idx]

    def changeFrame(self, other, idx):
        if not isinstance(other, VideoReader) or not other.isOpen or not self.isOpen:
            return None

        if idx == 0 and self.frameCount == 1:
            self._v = other[0]
        elif idx == 0:
            self._v = other[0] + self._v[1::]
        elif idx == self.frameCount - 1:
            self._v = self._v[:idx] + other[0]
        else:
            self._v = self._v[:idx] + other[0] + self._v[(idx + 1) :]

        self._set_video_params()

    def replaceClip(self, other):
        if not isinstance(other, vs.VideoNode):
            return None

        self._v = other[::]
        self._set_video_params()
        self._open = True

    def stackFramePlanes(self, frame):
        return DustScratch.api.image.VSFrameToImage(frame, self._v.format.num_planes)

    def makeClip(self, frame, width, height):
        return DustScratch.api.image.ImageToVSClip(frame, width, height)

    @property
    def clip(self):
        return self._v

    def _videoFromFrames(self, path):
        core = vs.core
        return core.imwri.Read(path)

    def _set_video_params(self):
        self._width = self._v.width
        self._height = self._v.height
        self._frame_count = self._v.num_frames
        self._fps = self._v.fps.numerator / self._v.fps.denominator
