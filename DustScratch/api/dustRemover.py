# DustScratch - Film Stock Editor
# Copyright (C) 2018 Michele Valsesia

import vapoursynth as vs

from DustScratch.api.filters import VSFilter
from DustScratch.util import timeit


@timeit
def removeDirt(clip, params):
    core = vs.core
    cleansed = core.rgvs.Clense(clip)
    sbegin = core.rgvs.ForwardClense(clip)
    send = core.rgvs.BackwardClense(clip)
    scenechange = core.rdvs.SCSelect(
        clip, sbegin, send, cleansed, dfactor=params.getParam("dfactor")
    )
    repmode = params.getParam("repmode")
    alt = core.rgvs.Repair(scenechange, clip, mode=[repmode, 1])
    restore = core.rgvs.Repair(cleansed, clip, mode=[repmode, 1])
    corrected = core.rdvs.RestoreMotionBlocks(
        cleansed,
        restore,
        neighbour=clip,
        alternative=alt,
        gmthreshold=params.getParam("gmthreshold"),
        dist=params.getParam("dist"),
        dmode=params.getParam("dmode"),
        noise=params.getParam("noise"),
        noisy=params.getParam("noisy"),
        tolerance=params.getParam("tolerance"),
        pthreshold=params.getParam("pthreshold"),
        cthreshold=params.getParam("cthreshold"),
        grey=params.getParam("grey"),
    )
    return core.rgvs.RemoveGrain(corrected, mode=[repmode])


class dustRemover(VSFilter):
    name = "remove_dust"

    default = {
        "dfactor": 4.0,
        "noise": 10,
        "noisy": 12,
        "repmode": 16,
        "gmthreshold": 80,
        "dist": 1,
        "tolerance": 12,
        "pthreshold": 10,
        "cthreshold": 10,
        "dmode": 2,
        "grey": 1,
    }

    setter = {
        "setDfactor": "dfactor",
        "setNoise": "noise",
        "setNoisy": "noisy",
        "setRepmode": "repmode",
        "setGmt": "gmthreshold",
        "setDist": "dist",
        "setTolerance": "tolerance",
        "setPth": "pthreshold",
        "setCth": "cthreshold",
        "setDmode": "dmode",
        "setGrey": "grey",
    }

    def filter(self):
        """ Convert clip to YUV420P8 """
        align2 = lambda n: int((n + 1) / 2) * 2
        clip = self.core.resize.Bicubic(
            clip=self.clipToFilter,
            width=align2(self.clipToFilter.width),
            height=align2(self.clipToFilter.height),
            format=vs.YUV420P8,
            matrix_s="709",
        )

        clip = removeDirt(clip, self)

        return self.videoOutput(clip)
