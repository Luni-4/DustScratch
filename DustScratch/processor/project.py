# DustScratch - Film Stock Editor
# Copyright (C) 2018 Michele Valsesia

import io
import pathlib
import sys

from PyQt5 import QtCore

import DustScratch.util


class NotValidProject(RuntimeError):
    pass


class ProjectProcessor(QtCore.QObject):

    loaded = QtCore.pyqtSignal()

    def __init__(self, gui):
        super().__init__()
        self._gui = gui
        self._path = None
        self._is_open = False

        self.project_params = []
        self.needs_save = False
        self.open_saved = False
        self.loaded_video_path = None

    def load_project(self, path):
        path = pathlib.Path(path)
        obj = DustScratch.util.unserialize_uncompress(path.read_bytes())
        self.loaded_video_path = obj["Video_Path"]
        self._gui.filters.filters_stack = obj["Actions"]
        self.project_params = obj["Filters_Params"]
        self._path = path
        self._is_open = False
        self.needs_save = False
        self.open_saved = True
        self.loaded.emit()

    def save_project(self, path):
        if not self.needs_save:
            return None

        path = pathlib.Path(path)
        if path.suffix not in [".ds"]:
            path = path.parent / pathlib.Path(path.name + ".ds")

        print(self.project_params)

        data = DustScratch.util.serialize_compress(
            {
                "Video_Path": self.loaded_video_path,
                "Actions": self._gui.filters.filters_stack,
                "Filters_Params": self.project_params,
            }
        )
        path.write_bytes(data)
        self.needs_save = False

    def new_project(self):
        self._path = None
        self._is_open = True
        self.project_params.clear()
        self.needs_save = False
        self.loaded_video_path = None

    def run_saved_project(self):
        self._is_open = True
        self._gui.filters.run_saved_filters()

    def add_params(self, name, params):
        self.project_params.append((name, params))

    def remove_params(self):
        self.project_params.pop()

    @property
    def path(self):
        return self._path

    @property
    def is_open(self):
        return self._is_open
