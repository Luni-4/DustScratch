# DustScratch - Film Stock Editor
# Copyright (C) 2018 Michele Valsesia

import abc

from PyQt5 import QtCore

from DustScratch.util import load_commands

_PATH = "DustScratch/filters"


class Filter(abc.ABC):
    def __init__(self, gui):
        self.gui = gui
        self.params = {}
        self.all = False

    @classmethod
    @property
    def name(cls):
        raise NotImplementedError("Filter has no name")

    @classmethod
    @property
    def default_params(cls):
        raise NotImplementedError("Filter has no default params")

    def set_params(self, params):
        self.params = params

    @property
    def all_frames(self):
        return self.all

    @all_frames.setter
    def all_frames(self, value):
        self.all = value

    @abc.abstractmethod
    def run(self):
        raise NotImplementedError("Filter has no implementation")


class FiltersProcessor(QtCore.QObject):

    filtered = QtCore.pyqtSignal()

    def __init__(self, gui):

        super().__init__()
        self._gui = gui
        self._filters_registry = {
            cls.name: cls for cls in load_commands(_PATH, "FILTERS")
        }
        self._saved_frame_idx = 0
        self._params_saver = {}

        self.filters_stack = []
        self.frames_filtered = {}

    def create_filter(self, name):
        cls = self._get_class(name)
        self._filter = cls(self._gui)

    def set_is_all(self, is_all):
        self._filter.all_frames = is_all

    def read_params(self, name):
        kargs = self.check_params(name)
        self._filter.set_params(kargs)

    def run_filter(self):
        self._all_frames()
        self._filter.run()
        self._gui.log.info(f"Run {self._filter.name}")
        if not self._gui.project.open_saved:
            self._gui.project.needs_save = True

        self.filtered.emit()

    def run_saved_filters(self):
        project_params = self._gui.project.project_params
        for idx, params in zip(self.filters_stack, project_params):
            print(params[0])
            print(idx)
            self.create_filter(params[0])
            if params[0] == "inpaint_filter":
                print(params[1][1])
                print(params[1][0])
                self._filter.set_mask(params[1][1])
                parameters = params[1][0]
            else:
                print(params[1])
                parameters = params[1]
            if idx == -1:
                self.set_is_all(True)
            else:
                self._saved_frame_idx = idx
            if bool(params[1]):
                self.save_current_params(params[0], parameters)
            self.read_params(params[0])
            self.run_filter()

    def set_frame_filtered(self, idx, frame):
        value = self.frames_filtered.get(idx, None)
        if value is None:
            self.frames_filtered[idx] = [frame]
        else:
            self.frames_filtered[idx].append(frame)

    def is_frame_filtered(self, value):
        if not self._gui.undo.filters_list.has_filters:
            return False

        return self.has_all or value in self.frames_filtered.keys()

    def frame_filtered(self, value):
        if self.has_all and value not in self.frames_filtered.keys():
            return self._gui.video.video_af.getFrame(value)

        return self.frames_filtered[value][-1].get_frame(0)

    def choose_clip(self):
        if self._filter.all_frames:
            self._all_to_filtered()
            return self._gui.video.video_af

        if self.has_all:
            return self._gui.video.video_af

        return self._gui.video.video_f

    def choose_save_mode(self, clip_filtered):
        if self._filter.all_frames:
            self._gui.video.video_af = clip_filtered
            return None

        clip = clip_filtered.clipFrame(self.saved_frame_index)

        if self.has_all:
            clip_o = self._gui.video.video_af.clipFrame(self.saved_frame_index)
            self.set_frame_filtered(self.saved_frame_index, clip_o)
            self._gui.video.video_af.changeFrame(clip, self.saved_frame_index)
        else:
            self._gui.video.video_f.changeFrame(clip, self.saved_frame_index)

        self.set_frame_filtered(self.saved_frame_index, clip)

    def remove_last_filter(self, filter_type):
        self._gui.project.remove_params()

        if filter_type == -1:
            self._all_to_filtered()
            return None

        if (
            not self._gui.undo.filters_list.has_filters
            or len(self.frames_filtered[filter_type]) == 1
        ):
            self._remove_frame(filter_type)
            clip = self._gui.video.video.clipFrame(filter_type)
            self._gui.video.video_f.changeFrame(clip, filter_type)
            return None

        if self.has_all:
            self._remove_frame(filter_type)
            self._gui.video.video_af.changeFrame(
                self.frames_filtered[filter_type][-1], filter_type
            )
            self._remove_frame(filter_type)
        else:
            self._remove_frame(filter_type)
            self._gui.video.video_f.changeFrame(
                self.frames_filtered[filter_type][-1], filter_type
            )

    def clean_filters(self):
        self.frames_filtered.clear()
        self.filters_stack.clear()
        self._params_saver.clear()

    def get_default_params(self, filter_name):
        return self._get_class(filter_name).default_params

    def save_current_params(self, filter_name, params):
        self._params_saver[filter_name] = params

    def get_current_params(self, filter_name):
        return self._params_saver[filter_name]

    def has_saved_params(self, filter_name):
        return filter_name in self._params_saver.keys()

    def check_params(self, filter_name):
        if self.has_saved_params(filter_name):
            return self.get_current_params(filter_name)
        return self.get_default_params(filter_name)

    @property
    def has_all(self):
        return -1 in self.filters_stack

    @property
    def saved_frame_index(self):
        if not self._gui.project.is_open:
            return self._saved_frame_idx
        return self._gui.video.frame_idx

    def _all_frames(self):
        if not self._gui.project.is_open:
            return None

        if self._filter.all_frames:
            self._filters_clip()
        else:
            self._filters_frame()

    def _filters_clip(self):
        if not self.has_all:
            self.filters_stack.append(-1)
            self._gui.project.add_params(
                self._filter.name, self.check_params(self._filter.name)
            )
            return None

        minus_one_idx = self.filters_stack.index(-1)
        remove = len(self.filters_stack[minus_one_idx:][1::])
        for idx in range(remove):
            self._gui.undo.filters_list.undo_filter()

        self._gui.project.remove_params()
        print(self.check_params(self._filter.name))
        self._gui.project.add_params(
            self._filter.name, self.check_params(self._filter.name)
        )

    def _filters_frame(self):
        self.filters_stack.append(self._gui.video.frame_idx)
        name = self._filter.name
        if name == "inpaint_filter":
            self._gui.project.add_params(
                name, (self.check_params(name), self._gui.video.mask)
            )
        else:
            self._gui.project.add_params(name, self.check_params(name))

    def _all_to_filtered(self):
        self._gui.video.video_af = self._gui.video.video_f

    def _remove_frame(self, idx):
        self.frames_filtered[idx].pop()
        if not self.frames_filtered[idx]:
            self.frames_filtered.pop(idx)

    def _get_class(self, name):
        return self._filters_registry[name]
