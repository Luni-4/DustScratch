# DustScratch - Film Stock Editor
# Copyright (C) 2018 Michele Valsesia


class UndoFilters:
    def __init__(self, gui, filters_stack):
        self._gui = gui
        self._filters_stack = filters_stack

    @property
    def has_filters(self):
        return len(self._filters_stack) > 0

    def undo_filter(self):
        if not self.has_filters:
            return None

        filter_type = self._filters_stack.pop()

        self._gui.filters.remove_last_filter(filter_type)

        if self.has_filters or self._gui.project.open_saved:
            self._gui.project.needs_save = True
        else:
            self._gui.project.needs_save = False

        self._gui.filters.filtered.emit()


class UndoProcessor:
    def __init__(self, gui):
        self._stack = [UndoFilters(gui, gui.filters.filters_stack)]

    @property
    def has_undo(self):
        return len(self._stack) > 1 or self._stack[0].has_filters

    @property
    def filters_list(self):
        return self._stack[0]

    def undo(self):
        last_action = self._stack[-1]
        if isinstance(last_action, UndoFilters):
            last_action.undo_filter()
