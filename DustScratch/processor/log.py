# DustScratch - Film Stock Editor
# Copyright (C) 2018 Michele Valsesia

import enum

from PyQt5 import QtCore


class LogLevel(enum.Enum):
    Error = 1
    Warning = 2
    Info = 3


class LogProcessor(QtCore.QObject):

    logged = QtCore.pyqtSignal(LogLevel, str)

    def info(self, msg):
        self.log(LogLevel.Info, msg)

    def warning(self, msg):
        self.log(LogLevel.Warning, msg)

    def error(self, msg):
        self.log(LogLevel.Error, msg)

    def log(self, level, msg):
        for line in msg.split("\n"):
            self.logged.emit(level, line)
