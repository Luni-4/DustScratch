# DustScratch - Film Stock Editor
# Copyright (C) 2018 Michele Valsesia

import enum
import json
import pathlib

from PyQt5 import QtCore

_DATA_PATH = "DustScratch/data/shortcuts.json"


class ShortcutContext(enum.Enum):
    Global = "global"


class Shortcut:
    def __init__(self, shortcut, command):

        self.shortcut = shortcut
        self.command = command


class ShortcutsProcessor(QtCore.QObject):
    def __init__(self):

        super().__init__()

        self.shortcuts = {ShortcutContext.Global: []}

        self._loads(pathlib.Path(_DATA_PATH).read_text())

    def _loads(self, text):
        obj = json.loads(text)
        for key, value in self.shortcuts.items():
            value.clear()
            value.extend(
                [
                    Shortcut(item["shortcut"], item["command"])
                    for item in obj.get(key.value, [])
                ]
            )
