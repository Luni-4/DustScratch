# DustScratch - Film Stock Editor
# Copyright (C) 2018 Michele Valsesia

from PyQt5 import QtCore, QtWidgets

from DustScratch.api.videoReader import VideoReader
from DustScratch.api.videoWriter import VideoWriter
from DustScratch.util import add_extension, split_parent_extension


class VideoProcessor(QtCore.QObject):

    opened = QtCore.pyqtSignal()
    frame_changed = QtCore.pyqtSignal()
    show_frame = QtCore.pyqtSignal()
    zoom_frame = QtCore.pyqtSignal()
    show_mode = QtCore.pyqtSignal()
    manual_mode = QtCore.pyqtSignal()
    mask_sig = QtCore.pyqtSignal()

    def __init__(self, gui):

        super().__init__()

        self._gui = gui

        self._video = VideoReader()
        self._frame_idx = 0
        self._curr_frame = None
        self._mask = None
        self._mode = False
        self._manual = False
        self._zoom = 1

        self._gui.filters.filtered.connect(self._show_filtered)

    def open_video(self, paths, seq=False):
        if seq:
            self._video.openVideo(paths, True)
        else:
            self._video.openVideo(paths[0])

        self.opened.emit()

    def create_filtered_clips(self):
        self.video_f = VideoReader()
        self.video_af = VideoReader()
        self.video_f.replaceClip(self._video.clip)
        self.video_af = self.video_f

    def save_video(self, path):

        progress = QtWidgets.QProgressDialog(
            "Save Video...", "Abort", 0, self.frame_count - 1, self._gui.main_window
        )

        progress.setWindowModality(QtCore.Qt.WindowModal)

        path = add_extension(path, "avi")

        video_w = VideoWriter()
        video_w.openVideo(
            path, fourcc="\0\0\0\0", fps=self.fps, width=self.width, height=self.height
        )

        for value in range(self.frame_count):
            progress.setValue(value)

            if progress.wasCanceled():
                return

            frame = self.get_plain_frame(value)
            video_w.writeFrame(frame)

        progress.setValue(self.frame_count - 1)
        video_w.closeVideo()

    def save_frames(self, path):
        indexes = self._save_frame_sequence(path)

        progress = QtWidgets.QProgressDialog(
            "Save Frame Sequence...",
            "Abort",
            0,
            self.frame_count - 1,
            self._gui.main_window,
        )

        progress.setWindowModality(QtCore.Qt.WindowModal)

        for idx in indexes:
            progress.setValue(idx)

            if progress.wasCanceled():
                return

        progress.setValue(self.frame_count - 1)

    def save_frame(self, path):
        path = add_extension(path, "png")
        frame = self.get_plain_frame(self.frame_idx)
        self._video.saveFrame(path, frame)

    def close_video(self):
        self.shutdown_video()
        self._clean_video()

    def shutdown_video(self):
        self._video.closeVideo()

    def set_manual(self, value):
        self._manual = value
        self.manual_mode.emit()

    def set_mask(self, mask):
        self._mask = mask

    def zoom_in(self):
        if self._zoom < 8:
            self._zoom = self._zoom * 2

        self.zoom_frame.emit()
        self.show_frame.emit()

    def zoom_out(self):
        if self._zoom > 1 / 8:
            self._zoom = self._zoom / 2

        self.zoom_frame.emit()
        self.show_frame.emit()

    def change_frame_view(self):
        self._mode = not self._mode
        self.get_frame(None)
        self.show_mode.emit()

    def get_frame(self, idx):

        if idx is not None and self._frame_idx != idx:
            self._frame_idx = idx

        self._curr_frame = self._get_frame_by_mode(self.frame_idx)

        if self._curr_frame is None:
            return

        self._curr_frame = self._video.stackFramePlanes(self._curr_frame)

        self.frame_changed.emit()

    def get_plain_frame(self, idx):
        frame = self._get_frame_by_mode(idx)
        return self._video.stackFramePlanes(frame)

    def convert_to_clip(self, frame):
        clip = self._video.makeClip(frame, self.width, self.height)
        video = VideoReader()
        video.replaceClip(clip)
        return video

    def _get_frame_by_mode(self, idx):
        if self.mode:
            return self._gui.filters.frame_filtered(idx)

        return self._video.getFrame(idx)

    def _show_filtered(self):
        self._mode = True
        self.get_frame(None)

    def _clean_video(self):
        self._curr_frame = None
        self._frame_idx = 0
        self._zoom = 1

    def _save_frame_sequence(self, path):
        idx = 0
        path = add_extension(path, "png")
        file_name, file_ext = split_parent_extension(path)
        while idx < self.frame_count:
            yield idx
            new_path = file_name + "_%02d" % idx + file_ext
            self._video.saveFrame(new_path, self.get_plain_frame(idx))
            idx += 1

    @property
    def video(self):
        return self._video

    @property
    def width(self):
        return self._video.width

    @property
    def height(self):
        return self._video.height

    @property
    def fps(self):
        return self._video.fps

    @property
    def is_open(self):
        return self._video.isOpen

    @property
    def frame_count(self):
        return self._video.frameCount

    @property
    def frame_idx(self):
        return self._frame_idx

    @property
    def curr_frame(self):
        return self._curr_frame

    @property
    def is_manual(self):
        return self._manual

    @property
    def mask(self):
        self.mask_sig.emit()
        return self._mask

    @property
    def zoom(self):
        return self._zoom

    @property
    def mode(self):
        is_filtered = self._gui.filters.is_frame_filtered(self.frame_idx)
        return is_filtered and self._mode
