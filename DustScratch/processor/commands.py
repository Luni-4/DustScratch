# DustScratch - Film Stock Editor
# Copyright (C) 2018 Michele Valsesia

import abc
import asyncio

from PyQt5 import QtCore

from DustScratch.util import load_commands

_PATH = "DustScratch/ui/commands"


class CommandError(RuntimeError):
    pass


class Command(abc.ABC):
    def __init__(self, gui, command_name):
        self.gui = gui
        self.command_name = command_name

    @classmethod
    @property
    def name(cls):
        pass

    @property
    def is_enabled(self):
        return True

    @abc.abstractmethod
    async def run(self):
        raise NotImplementedError("Command has no implementation")


class CommandsProcessor(QtCore.QObject):
    def __init__(self, gui):

        super().__init__()
        self._gui = gui
        self._commands = {cls.name: cls for cls in load_commands(_PATH, "COMMANDS")}

    def get_command(self, cmd_name):
        cls = self._commands.get(cmd_name, None)
        if not cls:
            raise CommandError("Command not found")
        return cls(self._gui, cmd_name)

    def run_command(self, cmd):
        asyncio.ensure_future(self.run_command_async(cmd))

    async def run_command_async(self, cmd):
        if cmd.is_enabled:
            await cmd.run()
        return True
