## DustScratch

*DustScratch* is a video editor mainly thought to restore old films.
It can repair damaged frames through image filtering and inpainting techniques.

**WATCH OUT**: *DustScratch* is not feature complete yet, so it's quite unstable.
Use it with discretion.

## Features

- Easily extend functions and internal filters with Python
- Provide an interface to add and remove external filters written as plugins
- Export videos and frames as uncompressed files
- Video Preview

## Installation

- Install system dependencies
  - `OpenCV`
  - `VapourSynth`
  - `QT5` and `PyQT5` bindings
- Clone the repository: `git clone https://gitlab.com/Luni-4/DustScratch`
- Enter its directory: `cd DustScratch`
- Install *DustScratch*: `pip install .`


## Acknowledgments

I would like to thank ([@rr-](https://github.com/rr-)) whose ideas helped me to
create the GUI system and ([@handaimaoh](https://github.com/handaimaoh)) for
the RemoveDirt plugin used as filter example.
